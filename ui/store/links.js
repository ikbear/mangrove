export const LINKS = {
  Mastodon: {
    icon: 'mdi-mastodon',
    link: 'https://mas.to/@mangroveReviews'
  },
  Twitter: {
    icon: 'mdi-twitter',
    link: 'https://twitter.com/mangroveReviews'
  },
  Riot: {
    img: require('~/static/icon-riot.svg'),
    link: 'https://riot.im/app/#/room/#mangrove:matrix.org'
  },
  Gitlab: {
    icon: 'mdi-gitlab',
    link: 'https://gitlab.com/plantingspace/mangrove'
  },
  OpenCollective: {
    link: 'https://opencollective.com/mangrove'
  }
}
