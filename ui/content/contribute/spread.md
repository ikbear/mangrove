---
title: Spread the word
---

Follow Mangrove and spread the word on social media, or write about Mangrove’s approach to open data and privacy. Help to grow the community and the ecosystem so that we free this valuable source of insight together, for the benefit of all.

