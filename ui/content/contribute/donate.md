---
title: Provide financial resources
---

Support us in maintaining the open dataset and open source infrastructure 
by donating or sponsoring the initiative. 
Mangrove is a non-profit project. All its funds are managed transparently 
on [Open Collective](https://opencollective.com/mangrove).
