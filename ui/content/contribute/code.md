---
title: Contribute code and design work
---

Help improve Mangrove’s core infrastructure and user experience. Discuss with us missing features in our Riot chat, and/or make pull requests on GitLab.

Areas: Mangrove Reviews Standard, Server, APIs, 
Web app, Cryptography-based identity, UI
