---
title: Help grow the ecosystem of open reviews
---

Help us bring open reviews to your favorite application! This could be:
Any Google Maps alternatives and navigation apps
Company and business listing sites and apps
Travel and tourism websites
Restaurants, hotels, tourist attractions
Online libraries and bookstores

Write us at mangrove(at)planting(dot)space, and we will reach out to the respective projects or companies. If they are open-source, you could make a pull request to their repository. Or just inform them directly about Mangrove’s open-source API and open dataset via forums, customer service, or social media.

