---
title: Download and use the dataset
---

Develop new aggregation or recommendation algorithms, do research, or provide reputation management services to your clients by downloading and working with the dataset.
The dataseet is licensed under [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/).  
