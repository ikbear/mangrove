Our aim is to create an open data ecosystem for reviews of places, companies, websites, books, and more. We provide open-source infrastructure that enables any application or website to integrate a reviews layer, and to benefit from a shared data pool that is created by the combined user base.   
   
Mangrove provides  
* A [technical standard](https://mangrove.reviews/standard) to ensure interoperability
* An open-source [API](https://docs.mangrove.reviews)
* An aggregation algorithm to clean and filter the data
* A downloadable open dataset licensed under [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)  

Mangrove is a non-profit service hosted and supported by a community of businesses, developers and interested parties around the world. **Contact us to discuss your integration.** 
