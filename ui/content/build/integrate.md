---
title: Integrate open reviews into your product or service
---

Mangrove is built for integration and offers a solid open-source infrastructure and well-documented API for you to leverage freely. 
Offer your users a seamlessly integrated experience of online reviews, without them ever having to leave your platform.
